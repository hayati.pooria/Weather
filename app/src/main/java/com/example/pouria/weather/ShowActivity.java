package com.example.pouria.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pouria.weather.Models.Forecast;
import com.example.pouria.weather.Models.YahooWeather;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ShowActivity extends AppCompatActivity {
    ListView my_list;
   TextView humidity;
   TextView wind_speed;
    ImageView  icon;
    TextView city;
    TextView country;
    TextView temp;
    TextView date;
    public Context mContext=this;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        Intent intent=getIntent();
        String city_name=intent.getStringExtra("city_name");
        progressDialog=new ProgressDialog(mContext);
        progressDialog.setTitle("loading");
        progressDialog.setMessage("please wait to progress");
        bindView();
        search(city_name);
    }

    void bindView(){

        icon=(ImageView)findViewById(R.id.icon);
        city=(TextView)findViewById(R.id.city);
        country=(TextView)findViewById(R.id.country);
        date=(TextView)findViewById(R.id.date);
        temp=(TextView)findViewById(R.id.temp);
        humidity=(TextView)findViewById(R.id.humidity);
        wind_speed=(TextView)findViewById(R.id.wind_speed);
        my_list=(ListView)findViewById(R.id.my_list);

    }
    void search(String word){
        String url="https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+word+"%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client=new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            try {
                showToast(mContext,throwable.toString());
            }catch (Exception e){
                showToast(mContext,e.toString());

            }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
            parseserverrespond(responseString);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }
        });

    }
     public void showToast(Context mContext,String text){
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }
    void parseserverrespond(String serverrisponse){
        Gson gson=new Gson();
        YahooWeather weather=gson.fromJson(serverrisponse,YahooWeather.class);
        if (weather.getQuery().getCount()==1){
        String tempvalue=weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        String cityvalue=weather.getQuery().getResults().getChannel().getLocation().getCity();
        String countryvalue=weather.getQuery().getResults().getChannel().getLocation().getCountry();
        String datevalue=weather.getQuery().getResults().getChannel().getItem().getCondition().getDate();
        String humidityvalue=weather.getQuery().getResults().getChannel().getAtmosphere().getHumidity();
        String speedvalue=weather.getQuery().getResults().getChannel().getWind().getSpeed();
        String statusvalue=weather.getQuery().getResults().getChannel().getItem().getCondition().getText();
        List <Forecast> ForecastModel=weather.getQuery().getResults().getChannel().getItem().getForecast();
        ForecastAdapter  adapter=new ForecastAdapter(mContext,ForecastModel);
        my_list.setAdapter(adapter);
       if (statusvalue.equals("Sunny"))
            icon.setImageResource(R.drawable.sunny);
       else  if (statusvalue.equals("Partly Sunny"))
            icon.setImageResource(R.drawable.sunny_period);
       else  if (statusvalue.contains(" Mostly Sunny"))
            icon.setImageResource(R.drawable.sunny_period);
       else if (statusvalue.contains("Cloudy"))
            icon.setImageResource(R.drawable.cloudy);
       else if (statusvalue.contains("Rain"))
            icon.setImageResource(R.drawable.rain);
       else if (statusvalue.contains("Snow"))
            icon.setImageResource(R.drawable.weather_snow);
       else
           icon.setImageResource(R.drawable.cloudy);

        city.setText(cityvalue);
        country.setText(countryvalue);
        temp.setText(toC(tempvalue)+" °C");
        date.setText(datevalue);
        wind_speed.setText("wind spd:"+" "+speedvalue+" "+"km/s");
        humidity.setText("humidity:"+" "+humidityvalue+""+"%");
    }
     else
         showToast(mContext,"check the name you have entered and try again");
     }

    public String toC(String F){
        Double FF=Double.parseDouble(F);
        double A =(FF-32)/1.8;
        int AA=(int)A;
    return AA + "";
    }
}
