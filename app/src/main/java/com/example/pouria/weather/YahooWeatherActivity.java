package com.example.pouria.weather;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class YahooWeatherActivity extends AppCompatActivity implements View.OnClickListener {
    Button search;
    EditText city;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yahoo_weather);
        bindView();
        search();
    }
    void bindView(){
        search=(Button)findViewById(R.id.search);
        city=(EditText)findViewById(R.id.city);
    }
    void search(){
        search.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.search){
        String cityvalue=city.getText().toString();
        Intent intent=new Intent(YahooWeatherActivity.this,ShowActivity.class);
        intent.putExtra("city_name",cityvalue);
        startActivity(intent);}


    }
    public void showToast(Context mContext, String text){
        Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
    }
}
