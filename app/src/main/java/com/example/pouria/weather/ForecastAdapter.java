package com.example.pouria.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pouria.weather.Models.Forecast;

import java.util.List;


public class ForecastAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast>forecasts;
    public  ForecastAdapter(Context mContext,List<Forecast>forecasts){
        this.forecasts=forecasts;
        this.mContext=mContext;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int i) {
        return forecasts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row= LayoutInflater.from(mContext).inflate(R.layout.forecast_list_item,viewGroup,false);
        TextView list_item1=row.findViewById(R.id.list_item1);
        TextView list_item2=row.findViewById(R.id.list_item2);
        TextView list_item3=row.findViewById(R.id.list_item3);
        ImageView img_icon=row.findViewById(R.id.img_icon);
        list_item1.setText(forecasts.get(i).getDay());
        list_item2.setText(toC(forecasts.get(i).getLow())+"°");
        list_item3.setText(toC(forecasts.get(i).getHigh())+"°");
        if (forecasts.get(i).getText().equals("Sunny"))
        img_icon.setImageResource(R.drawable.sunny);
        else  if (forecasts.get(i).getText().equals("Partly Sunny"))
            img_icon.setImageResource(R.drawable.sunny_period);
        else  if (forecasts.get(i).getText().equals("Mostly Sunny"))
            img_icon.setImageResource(R.drawable.sunny_period);
        else if (forecasts.get(i).getText().contains("Cloudy"))
            img_icon.setImageResource(R.drawable.cloudy);
       else if (forecasts.get(i).getText().contains("Rain"))
            img_icon.setImageResource(R.drawable.rain);
        else if (forecasts.get(i).getText().contains("Snow"))
            img_icon.setImageResource(R.drawable.weather_snow);
        else
            img_icon.setImageResource(R.drawable.cloudy);





        return row;
    }
    public String toC(String F){
        Double FF=Double.parseDouble(F);
        double A =(FF-32)/1.8;
        int AA=(int)A;
        return AA + "";
    }
}
